package mongodb

import (
	"context"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type Middleware struct {
	ConnectionString string
	DBName string
}

func (m *Middleware) NewMongoClient() (*mongo.Client, error)  {
	client, err := mongo.NewClient(options.Client().ApplyURI(m.ConnectionString))
	if err != nil {
		return nil, err
	}
	return client, nil
}

func (m *Middleware) GetMongoMiddleware() func (c *gin.Context) {
	return func (c *gin.Context) {
		client, err := m.NewMongoClient()
		if err != nil {
			log.Println(err)
		}
		connError := client.Connect(context.Background())
		if connError != nil {
			log.Println(err)
		}
		defer func() {
			err := client.Disconnect(context.Background())
			if err != nil {
				log.Println(err)
			}
		}()
		c.Set("dbClient", client)
		c.Set("dbName", m.DBName)
		c.Next()
	}
}