package mongodb

import (
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
)

type IFace interface {
	NewMongoClient() (*mongo.Client, error)
	GetMongoMiddleware() func (c *gin.Context)
}

func NewMongoMiddleware(conn string, dbName string) IFace {
	return &Middleware{
		ConnectionString: conn,
		DBName: dbName,
	}
}