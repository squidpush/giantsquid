package auth

import (
	"errors"
	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"gitlab.com/squidpush/giantsquid/helpers"
	"gitlab.com/squidpush/giantsquid/models"
	"gitlab.com/squidpush/giantsquid/repositories/userrepo"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"os"
	"time"
)

type Middleware struct {
	Realm          string
	Timeout        time.Duration
	SigningKey     []byte
	CookieDomain   string
	CookieHTTPOnly bool
	SecureCookie   bool
	SendCookie     bool
	CookieName     string
	TokenLookup    string
}

type login struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

const TEST_SIGNING_KEY = "test-signing-key"
const JWT_SIGNING_KEY_ENV = "JWT_SIGNING_KEY"

func (m *Middleware) getSigningKey() []byte {
	osEnvSigningKey := os.Getenv(JWT_SIGNING_KEY_ENV)
	if osEnvSigningKey != "" {
		return []byte(osEnvSigningKey)
	}
	return []byte(TEST_SIGNING_KEY)
}

func (m *Middleware) GetAuthMiddleware() *jwt.GinJWTMiddleware {
	tokenLookup := ""
	if m.TokenLookup == "" {
		tokenLookup = "header: Authorization, query: token, cookie: jwt"
	} else {
		tokenLookup = m.TokenLookup
	}
	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:      m.Realm,
		Key:        m.getSigningKey(),
		Timeout:    m.Timeout,
		MaxRefresh: time.Hour,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*models.UserProfile); ok {
				return jwt.MapClaims{
					"name":     v.Name,
					"photoUrl": v.PhotoUrl,
					"userId":   v.UserId,
				}
			}
			return jwt.MapClaims{}
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			userRepo, err := userrepo.GetUserRepoClient(c)
			if err != nil {
				// TODO: Use an error log here from the logging package
				log.Println("auth error: errors authenticating", err)
				return nil
			}
			userId := claims["userId"].(string)
			objectIdUserId, err := primitive.ObjectIDFromHex(userId)
			if err != nil {
				log.Println("auth error: errors authenticating", err)
				return nil
			}
			userProfile, err := userRepo.GetUserProfile(c, objectIdUserId)
			if err != nil {
				log.Println("auth error: errors authenticating", err)
				return nil
			}
			return userProfile
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {
			// get a user repo
			userRepo, err := userrepo.GetUserRepoClient(c)
			if err != nil {
				return nil, errors.New("auth error: errors authenticating")
			}
			var loginVals login
			if err := c.ShouldBind(&loginVals); err != nil {
				return "", jwt.ErrMissingLoginValues
			}
			username := loginVals.Username
			password := loginVals.Password
			user, err := userRepo.GetUserByUsername(c, username)
			if err != nil {
				return nil, errors.New("auth error: errors authenticating")
			}
			// Cast to the user model
			if helpers.CompareHash([]byte(user.Password), []byte(password), []byte(user.Salt)) {
				profile, err := userRepo.GetUserProfile(c, user.Id)
				return &profile, err
			}
			return nil, jwt.ErrFailedAuthentication
		},
		Authorizator: func(data interface{}, c *gin.Context) bool {
			if v, ok := data.(models.UserProfile); ok {
				// get a user repo
				userRepo, err := userrepo.GetUserRepoClient(c)
				if err != nil {
					log.Fatal(err)
					return false
				}
				userProfile, err := userRepo.GetUserProfile(c, v.UserId)
				if err != nil {
					log.Println(err)
					return false
				}
				// Ensure that the user retrived is not empty
				if userProfile.Name != "" {
					return true
				}
			}
			return false
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},
		// TokenLookup is a string in the form of "<source>:<name>" that is used
		// to extract token from the request.
		// Optional. Default value "header:Authorization".
		// Possible values:
		// - "header:<name>"
		// - "query:<name>"
		// - "cookie:<name>"
		// - "param:<name>"
		SendCookie:     m.SendCookie,
		SecureCookie:   m.SecureCookie,   //non HTTPS dev environments
		CookieHTTPOnly: m.CookieHTTPOnly, // JS can't modify
		CookieDomain:   m.CookieDomain,
		CookieName:     m.CookieName, // default jwt
		TokenLookup:    tokenLookup,
		// TokenLookup: "query:token",
		// TokenLookup: "cookie:token",

		// TokenHeadName is a string in the header. Default value is "Bearer"
		TokenHeadName: "Bearer",

		// TimeFunc provides the current time. You can override it to use another time value. This is useful for testing or if your server uses a different time zone than your tokens.
		TimeFunc: time.Now,
	})

	if err != nil {
		log.Fatal(err)
		return nil
	}
	return authMiddleware
}

type Claims struct {
	Name     string             `json:"name"`
	UserId   primitive.ObjectID `json:"userId"`
	PhotoUrl string             `json:"photoUrl"`
}

func GetClaims(c *gin.Context) *Claims {
	claims := jwt.ExtractClaims(c)
	var name string
	var photoUrl string
	var objId primitive.ObjectID
	var err error
	if v, ok := claims["name"].(string); ok {
		name = v
	}
	if v, ok := claims["photoUrl"].(string); ok {
		photoUrl = v
	}
	if v, ok := claims["userId"].(string); ok {
		objId, err = primitive.ObjectIDFromHex(v)
		if err != nil {
			log.Println("error casting to object id")
		}
	}
	return &Claims{
		name,
		objId,
		photoUrl,
	}
}
