package auth

import (
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/http"
	"github.com/stretchr/testify/require"
	"gitlab.com/squidpush/giantsquid/repositories/userrepo"
	"testing"
)

func TestSignUp_Success(t *testing.T) {
	w := http.TestResponseWriter{}
	c, _ := gin.CreateTestContext(&w)
	mockUserRepo := userrepo.MockIFace{}
	payload := SignUpPayload{
		Email:           "test@test.com",
		Name:            "TestUser",
		Username:        "TestUsername",
		Password:        "TestPassword",
		ConfirmPassword: "TestPassword",
	}
	mockUserRepo.On("CreateUser", c, payload.Name, payload.Username, payload.Email, payload.Password).
				 Return(nil)
	err := signUp(c, &payload, &mockUserRepo)
	require.Nil(t, err)
}

func TestSignUp_PasswordMismatch(t *testing.T) {
	w := http.TestResponseWriter{}
	c, _ := gin.CreateTestContext(&w)
	mockUserRepo := userrepo.MockIFace{}
	payload := SignUpPayload{
		Email:           "test@test.com",
		Name:            "TestUser",
		Username:        "TestUsername",
		Password:        "TestPassword",
		ConfirmPassword: "TestPasswordDifferent",
	}
	mockUserRepo.On("CreateUser", c, payload.Name, payload.Username, payload.Email, payload.Password).
		Return(nil)
	err := signUp(c, &payload, &mockUserRepo)
	require.NotNil(t, err)
	require.Equal(t,"passwords do not match", err.Error())
}
