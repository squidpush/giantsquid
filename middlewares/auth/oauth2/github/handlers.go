package github

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/caarlos0/env"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	gogit "github.com/google/go-github/github"
	"gitlab.com/squidpush/giantsquid/middlewares/auth"
	"gitlab.com/squidpush/giantsquid/repositories/userrepo"
	"gitlab.com/squidpush/giantsquid/types"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/github"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"
)

type GitHubOAuth struct {
	redirectUri string
	tokenDuration time.Duration
	conf *oauth2.Config
	cookieAuthConfig *types.SquidCookieAuthConfig
}

func (g *GitHubOAuth) getSigningKey() []byte {
	osEnvSigningKey := os.Getenv(auth.JWT_SIGNING_KEY_ENV)
	if osEnvSigningKey != "" {
		return []byte(osEnvSigningKey)
	}
	return []byte(auth.TEST_SIGNING_KEY)
}

func (g *GitHubOAuth) GitHubGrantHandler(c *gin.Context) {
	// Use the custom HTTP client when requesting a token.
	code := c.Query("code")
	if code == "" {
		c.JSON(http.StatusFailedDependency, gin.H{
			"error": "no exchange code",
		})
		return
	}
	httpClient := &http.Client{Timeout: 2 * time.Second}
	ctx := context.WithValue(c, oauth2.HTTPClient, httpClient)
	tok, err := g.conf.Exchange(ctx, code)
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusConflict, gin.H{
			"error": "could not login via github",
		})
		return
	}
	client := g.conf.Client(ctx, tok)
	gc := gogit.NewClient(client)
	user, _, err := gc.Users.Get(oauth2.NoContext, "")
	if err != nil {
		c.JSON(http.StatusConflict, gin.H{
			"error": err.Error(),
		})
		return
	}
	if user == nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "github user does not exist",
		})
		return
	}
	// Check if GitHub user already exists
	cl, err := userrepo.GetUserRepoClient(c)
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "something went wrong on the request",
		})
		return
	}
	userProfile, err := cl.GetOrCreateGitHubUser(c, user)
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "something went wrong on the request",
		})
		return
	}
	jwtc := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"name": userProfile.Name,
		"photoUrl": userProfile.PhotoUrl,
		"userId": userProfile.UserId,
		"exp": float64(time.Now().Unix() + int64(g.tokenDuration.Seconds())),
	})
	ss, err := jwtc.SignedString(g.getSigningKey())
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusExpectationFailed, gin.H{
			"message": "failed to create token",
		})
		return
	}
	// TODO: set cookie here instead in production
	if g.cookieAuthConfig != nil {
		c.SetCookie(
			g.cookieAuthConfig.CookieName,
			ss,
			int(time.Hour.Seconds()),
			"/",
			g.cookieAuthConfig.CookieDomain,
			g.cookieAuthConfig.SecureCookie,
			g.cookieAuthConfig.CookieHTTPOnly)
		c.Redirect(http.StatusPermanentRedirect, g.redirectUri)
		return
	}
	c.Redirect(http.StatusPermanentRedirect, fmt.Sprintf("%s?token=%s", g.redirectUri, ss))
	return
}

func (g *GitHubOAuth) RedirectToGitHubHandler(c *gin.Context) {
	state := string(rand.Int63())
	url := g.conf.AuthCodeURL(state, oauth2.AccessTypeOffline)
	c.Redirect(http.StatusTemporaryRedirect, url)
	return
}

type githubOauthConfig struct {
	RedirectUri string `json:"redirectUri" env:"GITHUB_REDIRECT_URI"`
	ClientId   string `json:"clientId" env:"GITHUB_CLIENT_ID"`
	Secret	string `json:"secret" env:"GITHUB_SECRET"`
}

func NewGitHubOauth(
	scopes []string,
	useEnv bool,
	duration *time.Duration,
	cookieAuthConfig *types.SquidCookieAuthConfig) *GitHubOAuth {
	// json data
	gconf := githubOauthConfig{}
	if !useEnv {
		// read file
		conf, err := ioutil.ReadFile("./github-oauth.json")
		if err != nil {
			log.Fatal("please set credentials file")
		}
		err = json.Unmarshal(conf, &gconf)
		if err != nil {
			log.Fatal("error:", err)
		}
	} else {
		err := env.Parse(&gconf)
		if err != nil {
			log.Fatal("error:", err)
		}
	}
	return &GitHubOAuth{
		redirectUri : gconf.RedirectUri,
		tokenDuration: *duration,
		conf: &oauth2.Config{
			ClientID:     gconf.ClientId,
			ClientSecret: gconf.Secret,
			Scopes:       scopes,
			Endpoint: github.Endpoint,
		},
		cookieAuthConfig: cookieAuthConfig,
	}
}