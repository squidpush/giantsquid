package auth

import (
	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
)

type IFace interface {
	GetAuthMiddleware(jwtSigningKey []byte) *jwt.GinJWTMiddleware
}

// helper methods
func GetNameFromClaims(c *gin.Context) string {
	claims := jwt.ExtractClaims(c)
	if v, ok := claims["name"].(string); ok {
		return v
	}
	return ""
}