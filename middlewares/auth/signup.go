package auth

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/squidpush/giantsquid/repositories/userrepo"
	"log"
	"net/http"
)

type SignUpPayload struct {
	Email 	 string `json:"email,omitempty" bson:"email,omitempty"`
	Name 	 string `json:"name" bson:"name" binding:"required"`
	Username string `json:"username" bson:"username" binding:"required"`
	Password string `json:"password" bson:"password" binding:"required"`
	ConfirmPassword string `json:"confirmPassword" bson:"confirmPassword" binding:"required"`
}

func signUp(c *gin.Context, payload *SignUpPayload, cl userrepo.IFace) error {
	if payload.Password != payload.ConfirmPassword {
		return errors.New("passwords do not match")
	}
	err := cl.CreateUser(c, payload.Name, payload.Username, payload.Email, payload.Password)
	if err != nil {
		log.Println(err)
		return errors.New("something went wrong on the request")
	}
	return nil
}

// SignUpHandler is a sample signup handler
func SignUpHandler(c *gin.Context) {
	var payload SignUpPayload
	var err error
	if err = c.BindJSON(&payload); err == nil {
		var cl userrepo.IFace
		cl, err = userrepo.GetUserRepoClient(c)
		if err != nil {
			log.Println(err)
			c.JSON(http.StatusBadRequest, gin.H{
				"message": "something went wrong on the request",
			})
			return
		}
		err = signUp(c, &payload, cl)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": err.Error(),
			})
			return
		}
		c.JSON(http.StatusOK, gin.H{
			"message": "created user",
		})
		return
	}
	log.Println(err)
	c.JSON(http.StatusBadRequest, gin.H{
		"Message": err.Error(),
	})
	return
}