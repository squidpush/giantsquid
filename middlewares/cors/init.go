package cors

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

type middleware struct {
	corsConfig *cors.Config
}

func (m *middleware) NewCorsMiddleware() gin.HandlerFunc {
	return cors.New(*m.corsConfig)
}

func GetCorsMiddleware(cc *cors.Config) gin.HandlerFunc {
	m := &middleware{
		corsConfig: cc,
	}
	return m.NewCorsMiddleware()
}