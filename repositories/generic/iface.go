package generic

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
)

type IFace interface {
	Close(ctx context.Context)
	GetAll(ctx context.Context, limit int64) *mongo.Cursor
	FindById(ctx context.Context, Id string)*mongo.SingleResult
	FindByKV(ctx context.Context, Key string, Value interface{}) *mongo.Cursor
	Update(ctx context.Context, Id string, updateParams interface{}) error
	Delete(ctx context.Context, Id string) *mongo.DeleteResult
	Insert(ctx context.Context, newModel interface{}) *mongo.InsertOneResult
}

func NewGenericClient(collection *mongo.Collection, cl *mongo.Client) IFace {
	return &mongoRepo{genericCollection: collection, cl: cl}
}


