package generic

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type mongoRepo struct {
	genericCollection *mongo.Collection
	cl                *mongo.Client
}

func getObjectId(Id string) primitive.ObjectID {
	objectId, err := primitive.ObjectIDFromHex(Id)
	if err != nil {
		log.Println(err)
	}
	return objectId
}

func (m *mongoRepo) Close(ctx context.Context) {
	err := m.cl.Disconnect(ctx)
	if err != nil {
		log.Fatal(err)
	}
}

func (m *mongoRepo) GetAll(ctx context.Context, limit int64) *mongo.Cursor {
	findOptions := options.Find()
	if limit > 0 {
		findOptions.SetLimit(limit)
	}
	cursor, err := m.genericCollection.Find(ctx, bson.M{}, findOptions)
	if err != nil {
		log.Println("An error has occurred", err)
		return nil
	}
	return cursor
}

func (m *mongoRepo) Insert(ctx context.Context, newModel interface{}) *mongo.InsertOneResult {
	insertResult, err := m.genericCollection.InsertOne(ctx, newModel)
	if err != nil {
		log.Println("An error has occurred inserting", err)
	}
	return insertResult
}

func (m *mongoRepo) FindById(ctx context.Context, Id string) *mongo.SingleResult {
	singleResult := m.genericCollection.FindOne(ctx, bson.M{
		"_id": getObjectId(Id),
	})
	return singleResult
}

func (m *mongoRepo) FindByKV(ctx context.Context, Key string, Value interface{}) *mongo.Cursor {
	cursor, err := m.genericCollection.Find(ctx, bson.M{
		Key: Value,
	})
	if err != nil {
		log.Println("An error has occurred", err)
		return nil
	}
	return cursor
}

func (m *mongoRepo) Update(ctx context.Context, Id string, updateParams interface{}) error {
	filter := bson.M{"_id": bson.M{"$eq": getObjectId(Id)}}
	update := bson.M{"$set": updateParams}
	_, err := m.genericCollection.UpdateOne(ctx, filter, update)
	if err != nil {
		log.Println("An error has occurred updating", err)
	}
	return err
}

func (m *mongoRepo) Delete(ctx context.Context, Id string) *mongo.DeleteResult {
	deleteResult, err := m.genericCollection.DeleteOne(ctx, bson.M{
		"_id": getObjectId(Id),
	})
	if err != nil {
		log.Println("An error has occurred deleting", err)
	}
	return deleteResult
}
