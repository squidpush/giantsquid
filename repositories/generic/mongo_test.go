package generic

import (
	"context"
	"gitlab.com/squidpush/giantsquid/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"reflect"
	"testing"
)

func Test_getObjectId(t *testing.T) {
	type args struct {
		Id string
	}
	expected := primitive.NewObjectID()
	tests := []struct {
		name string
		args args
		want primitive.ObjectID
	}{
		{"getObjectId converts a newObjectId to string", args{expected.Hex()}, expected},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getObjectId(tt.args.Id); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getObjectId() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mongoRepo_Close(t *testing.T) {
	m := MockIFace{}
	m.On("Close", context.Background()).Return(nil)
	// ctx, _ := context.WithTimeout(context.Background(), 1000)
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name   string
		args   args
	}{
		{"Close is called  with the right params", args{context.Background()}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m.Close(tt.args.ctx)
		})
	}
}

func Test_mongoRepo_Delete(t *testing.T) {
	type args struct {
		ctx context.Context
		Id  string
	}
	m := MockIFace{}
	m.On("Delete", context.Background(), "123").Return(nil)
	tests := []struct {
		name   string
		args   args
		want   *mongo.DeleteResult
	}{
		{"Delete is called with the right params",  args{context.Background(), "123"}, nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := m.Delete(tt.args.ctx, tt.args.Id); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Delete() = %v, want %v", got, tt.want)
			}
		})
	}
}


func Test_mongoRepo_FindById(t *testing.T) {
	m := MockIFace{}
	m.On("FindById", context.Background(), "123").Return(nil)
	type args struct {
		ctx context.Context
		Id  string
	}
	tests := []struct {
		name   string
		args   args
		want   *mongo.SingleResult
	}{
		{"FindById is called with right params",  args{context.Background(), "123"}, nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := m.FindById(tt.args.ctx, tt.args.Id); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindById() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mongoRepo_FindByKV(t *testing.T) {
	m := MockIFace{}
	m.On("FindByKV", context.Background(), "username", "nisarga").Return(nil)
	type args struct {
		ctx   context.Context
		Key   string
		Value interface{}
	}
	tests := []struct {
		name   string
		args   args
		want   *mongo.Cursor
	}{
		{"FindByKV is called with right params",  args{context.Background(), "username", "nisarga"}, nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := m.FindByKV(tt.args.ctx, tt.args.Key, tt.args.Value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindByKV() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mongoRepo_GetAll(t *testing.T) {
	m := MockIFace{}
	m.On("GetAll", context.Background(), int64(0)).Return(nil)
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name   string
		args   args
		want   *mongo.Cursor
	}{
		{"FindByKV is called with right params",  args{context.Background()}, nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := m.GetAll(tt.args.ctx, int64(0)); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetAll() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mongoRepo_Insert(t *testing.T) {
	m := MockIFace{}
	m.On("Insert", context.Background(), models.User{}).Return(nil)
	type args struct {
		ctx      context.Context
		newModel interface{}
	}
	tests := []struct {
		name   string
		args   args
		want   *mongo.InsertOneResult
	}{
		{"Insert is called with right params",  args{context.Background(), models.User{}}, nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			if got := m.Insert(tt.args.ctx, tt.args.newModel); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Insert() = %v, want %v", got, tt.want)
			}
		})
	}
}


func Test_mongoRepo_Update(t *testing.T) {
	m := MockIFace{}
	m.On("Update", context.Background(), "123", nil).Return(nil)
	type args struct {
		ctx          context.Context
		Id           string
		updateParams interface{}
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"Update is called with right params",  args{context.Background(), "123", nil}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := m.Update(tt.args.ctx, tt.args.Id, tt.args.updateParams); (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}