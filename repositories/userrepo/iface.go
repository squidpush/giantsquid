package userrepo

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/google/go-github/github"
	"gitlab.com/squidpush/giantsquid/helpers"
	"gitlab.com/squidpush/giantsquid/models"
	"gitlab.com/squidpush/giantsquid/repositories/generic"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IFace interface {
	// GetUser gets a user with authentication data
	GetUser(ctx context.Context, userId primitive.ObjectID) (models.User, error)
	// GetUserByKV gets the user by key value
	GetUserByUsername(ctx context.Context, username string) (models.User, error)
	// GetOrCreateGitHubUser gets or create a github user - used with Oauth
	GetOrCreateGitHubUser(ctx context.Context, githubUser *github.User) (*models.UserProfile, error)
	// GetUserProfileByGitHubId gets a user profile by a provided github id
	GetUserProfileByGitHubId(ctx context.Context, githubId string) (models.UserProfile, error)
	// CreateUser creates a blank profile and a new user
	CreateUser(ctx context.Context, name string, username string, email string, password string) error
	// DeleteUser deletes the user profile and the user
	DeleteUser(ctx context.Context, userId primitive.ObjectID) error
	// UpdateUserPassword updates the user's password
	UpdateUserPassword(ctx context.Context, userId primitive.ObjectID, oldPassword string, newPassword string) error
	// GetUserProfile gets a user's profile
	GetUserProfile(ctx context.Context, userId primitive.ObjectID) (models.UserProfile, error)
	// UpdateUserProfile updates a user's profile given their userId and params to update
	UpdateUserProfile(ctx context.Context, userId primitive.ObjectID, updateParams interface{}) error
	// Close the clients
	Close(ctx context.Context)
}

func newUserRepoClient(usersCollectionClient generic.IFace, userProfilesCollectionClient generic.IFace) IFace {
	return &userRepo{
		usersCollectionClient:        usersCollectionClient,
		userProfilesCollectionClient: userProfilesCollectionClient,
	}
}

func GetUserRepoClient(c *gin.Context) (IFace, error) {
	usersCollectionClient, err := helpers.GetCollectionClient(c, "users")
	if err != nil {
		return nil, err
	}
	userProfilesCollectionClient, err := helpers.GetCollectionClient(c, "userProfiles")
	if err != nil {
		return nil, err
	}
	return newUserRepoClient(usersCollectionClient, userProfilesCollectionClient), nil
}