package userrepo

import (
	"context"
	"errors"
	"github.com/google/go-github/github"
	"gitlab.com/squidpush/giantsquid/helpers"
	"gitlab.com/squidpush/giantsquid/models"
	"gitlab.com/squidpush/giantsquid/repositories/generic"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"strconv"
	"time"
)

type userRepo struct {
	// generic client that relates to the users table
	usersCollectionClient generic.IFace
	// generic client that relates to the userProfiles table
	userProfilesCollectionClient generic.IFace
}

// CreateUser checks if the user with that username exists, hashes password and stores in db
func (u *userRepo) CreateUser(ctx context.Context, name string, username string, email string, password string) error {
	// Check if user exists with that username
	user, err := u.GetUserByUsername(ctx, username)
	if err != nil && err != mongo.ErrNilCursor {
		return err
	}
	if err == nil && user.Username != "" {
		return errors.New("invalid user creation: user with that username exists")
	}

	// General authentication process:
	// 1) Generate a salt, generate an argon2 hash

	// Get the salt
	salt := helpers.GenSalt()

	// Hash password
	hashedPassword := helpers.GenHash([]byte(password), salt)

	userId := primitive.NewObjectID()

	// Create a new user
	newUser := models.User{
		Id:       userId,
		Username: username,
		Email:    email,
		Password: string(hashedPassword),
		Salt:     string(salt),
	}

	// Attempt to insert this new model
	insertUserResult := u.usersCollectionClient.Insert(ctx, newUser)
	if insertUserResult.InsertedID == nil {
		return errors.New("invalid user creation: user could not be created")
	}

	// Create a new user profile
	newUserProfile := models.UserProfile{
		Id:           primitive.NewObjectID(),
		UserId:       userId,
		Name:         name,
	}
	insertUserProfileResult := u.userProfilesCollectionClient.Insert(ctx, newUserProfile)
	if insertUserProfileResult.InsertedID == nil {
		return errors.New("invalid user creation: user profile could not be created")
	}

	// success so no error
	return nil
}

func (u *userRepo) GetOrCreateGitHubUser(ctx context.Context, githubUser *github.User) (*models.UserProfile, error) {
	githubId := strconv.Itoa(int(githubUser.GetID()))
	squidUserProfile, err := u.GetUserProfileByGitHubId(ctx, githubId)
	if err != nil && err != mongo.ErrNilCursor {
		return nil, err
	}
	if err == nil && squidUserProfile.GitHubId != "" {
		return &squidUserProfile, nil
	}
	// github squid user does not exist
	userId := primitive.NewObjectID()
	// Create a new user
	newUser := models.User{
		Id:       userId,
		GitHubId: githubId,
		Username: githubUser.GetLogin() + strconv.Itoa(int(time.Now().Unix())),
		Email:    githubUser.GetEmail(),
	}
	// Attempt to insert this new model
	insertUserResult := u.usersCollectionClient.Insert(ctx, newUser)
	if insertUserResult.InsertedID == nil {
		return nil, errors.New("invalid user creation: user could not be created")
	}

	// Get name if it's empty set it to their github username
	name := githubUser.GetName()
	if name == "" {
		name = githubUser.GetLogin()
	}

	// Create a new user profile
	newUserProfile := models.UserProfile{
		Id:           primitive.NewObjectID(),
		GitHubId:     githubId,
		UserId:       userId,
		Name:         name,
		PhotoUrl:     githubUser.GetAvatarURL(),
		Bio: 		  githubUser.GetBio(),
		LocatedAt:    githubUser.GetLocation(),
	}

	insertUserProfileResult := u.userProfilesCollectionClient.Insert(ctx, newUserProfile)
	if insertUserProfileResult.InsertedID == nil {
		return nil, errors.New("invalid user creation: user profile could not be created")
	}
	return &newUserProfile, nil
}

func (u *userRepo) GetUser(ctx context.Context, userId primitive.ObjectID) (models.User, error) {
	user := models.User{}
	err := u.usersCollectionClient.FindById(ctx, userId.String()).Decode(&user)
	if err != nil {
		return models.User{}, err
	}
	if user.Username == "" {
		return models.User{}, errors.New("invalid get: could not get user")
	}
	return user, nil
}

func (u *userRepo) GetUserByUsername(ctx context.Context, username string) (models.User, error)  {
	users := make([]models.User, 0)
	err := u.usersCollectionClient.FindByKV(ctx, "username", username).All(ctx, &users)
	if err == nil && len(users) == 0 {
		return models.User{}, nil
	}
	if err != nil {
		return models.User{}, err
	}
	if users[0].Username == "" {
		return users[0], errors.New("invalid get: could not get user")
	}
	return users[0], nil
}

func (u *userRepo) GetUserProfileByGitHubId(ctx context.Context, githubId string) (models.UserProfile, error) {
	userProfiles := make([]models.UserProfile, 0)
	err := u.userProfilesCollectionClient.FindByKV(ctx, "githubId", githubId).All(ctx, &userProfiles)
	if err == nil && len(userProfiles) == 0 {
		return models.UserProfile{}, nil
	}
	if err != nil {
		return models.UserProfile{}, err
	}
	if userProfiles[0].GitHubId == "" {
		return userProfiles[0], errors.New("invalid get: could not get user")
	}
	return userProfiles[0], nil
}

// Payload passed to the generic db client update method for password
type PasswordUpdate struct {
	Password string `json:"password" bson:"password"`
	Salt 	 string `json:"salt" bson:"salt"`
}

func (u *userRepo) UpdateUserPassword(ctx context.Context,
	                                  userId primitive.ObjectID,
	                                  oldPassword string,
	                                  newPassword string) error {
	user, err := u.GetUser(ctx, userId)
	if err != nil {
		return err
	}
	if helpers.CompareHash([]byte(oldPassword), []byte(user.Password), []byte(user.Salt)) {
		newSalt := helpers.GenSalt()
		hashedPassword := helpers.GenHash([]byte(newPassword), newSalt)
		updateParams := PasswordUpdate{
			Password: string(hashedPassword),
			Salt:     string(newSalt),
		}
		updateErr := u.usersCollectionClient.Update(ctx, user.Id.String(),  updateParams)
		if updateErr != nil {
			return updateErr
		}
		return nil
	}
	return errors.New("invalid update: could not update user password")
}

func (u *userRepo) DeleteUser(ctx context.Context, userId primitive.ObjectID) error {
	// get a user profile
	userProfile, err := u.GetUserProfile(ctx, userId)
	if err != nil {
		return err
	}

	// delete it
	deleteUserProfileResult := u.userProfilesCollectionClient.Delete(ctx, userProfile.Id.String())
	if deleteUserProfileResult.DeletedCount != 1 {
		return errors.New("invalid delete: could not delete user profile")
	}

	user, err := u.GetUser(ctx, userId)
	if err != nil {
		return err
	}

	deleteUserResult := u.usersCollectionClient.Delete(ctx, user.Id.String())
	if deleteUserResult.DeletedCount != 1 {
		return errors.New("invalid delete: could not delete user")
	}

	// success so no error
	return nil
}

func (u *userRepo) GetUserProfile(ctx context.Context, userId primitive.ObjectID) (models.UserProfile, error) {
	profiles := make([]models.UserProfile, 0)
	// Delete the user profile, first get it
	err := u.userProfilesCollectionClient.FindByKV(ctx, "_user_id", userId).All(ctx, &profiles)
	if err == nil && len(profiles) == 0 {
		return models.UserProfile{}, nil
	}
	if err != nil {
		return models.UserProfile{}, err
	}
	if profiles[0].Name == "" {
		return profiles[0], errors.New("invalid get: could not get user profile")
	}
	return profiles[0], nil
}


// TODO: (good enough for now) Make this better i.e. compare old user profile with new one and update based upon that
func (u *userRepo) UpdateUserProfile(ctx context.Context, userId primitive.ObjectID, updateParams interface{}) error {
	// Get the user profile
	userProfile, err := u.GetUserProfile(ctx, userId)
	if err != nil {
		return err
	}
	updateErr := u.userProfilesCollectionClient.Update(ctx, userProfile.Id.String(), updateParams)
	if updateErr != nil {
		return updateErr
	}
	return nil
}

func (u *userRepo) Close(ctx context.Context) {
	u.userProfilesCollectionClient.Close(ctx)
	u.usersCollectionClient.Close(ctx)
}