package generators

import (
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/http"
	"github.com/stretchr/testify/require"
	"gitlab.com/squidpush/giantsquid/repositories/generic"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"reflect"
	"testing"
)

type TestProject struct {
	Id 	 primitive.ObjectID `bson:"_id,omitempty"`
	Name string `bson:"name"`
	Description string `bson:"description"`
	Author string `bson:"author"`
}

func TestCrud_create(t *testing.T) {
	w := http.TestResponseWriter{}
	c, _ := gin.CreateTestContext(&w)
	model := TestProject{
		Id: primitive.NewObjectID(),
		Name: "Toroban",
		Description: "Toroidal Sokoban",
		Author: "Eric",
	}
	crc := CrudConfig{
		Collection:   "projects",
		UniqueFields: nil,
		Model:        reflect.TypeOf(model),
		Payload:      CrudPayload{},
	}
	cr := NewCrudAPI(&crc)
	m := &generic.MockIFace{}
	value := reflect.ValueOf(model)
	m.On("Insert", c, value.Interface()).Return(&mongo.InsertOneResult{InsertedID:1})
	err := cr.create(c, m, value)
	require.Nil(t, err)
}