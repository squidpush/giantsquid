package generators

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/squidpush/giantsquid/helpers"
	"gitlab.com/squidpush/giantsquid/middlewares/auth"
	"gitlab.com/squidpush/giantsquid/repositories/generic"
	"gitlab.com/squidpush/giantsquid/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"net/http"
	"reflect"
)

// CRUD API Generator

type CrudPayload struct {
	Update interface{}
	Delete interface{}
}

type ReadByIdPayload struct {
	Id string `uri:"id" binding:"required"`
}

type ReadByKVPayload struct {
	Value string `form:"value" binding:"required"`
}

type ReadAllPayload struct {
	Limit int64 `form:"limit"`
}

type UpdatePayload struct {
	Id           string                 `json:"id" binding:"required"`
	UpdateParams map[string]interface{} `json:"updateParams" binding:"required"`
}

type DeletePayload struct {
	Id string `uri:"id" binding:"required"`
}

type CrudConfig struct {
	// Collection to create Crud API for
	Collection string
	// Unique Fields
	UniqueFields []string
	// Model
	Model interface{}
	// Request payloads
	Payload CrudPayload
	// Create modifier
	OwnerIdField string
	// Updateable fields
	UpdateableDBFields []string
	// Findable fields
	FindableDBFields []string
	// owners check opt out
	OwnerCheckOptOut bool
	// updateableFieldSet
	updateableFieldSet map[string]bool
}

type crud struct {
	config *CrudConfig
}

func (crudApi *crud) GetCollectionClient(c *gin.Context) generic.IFace {
	cl, err := helpers.GetCollectionClient(c, crudApi.config.Collection)
	if err != nil {
		log.Println(err)
		return nil
	}
	return cl
}

func (crudApi *crud) getDBFieldFromModel(modelField string) (string, error) {
	mt := reflect.TypeOf(crudApi.config.Model)
	mtf, exists := mt.FieldByName(modelField)
	if !exists {
		return "", errors.New("model field does not exist")
	}
	return mtf.Tag.Get("bson"), nil
}

func (crudApi *crud) create(c *gin.Context, cl generic.IFace, model reflect.Value) error {
	for _, field := range crudApi.config.UniqueFields {
		var value []primitive.M
		var findValue string
		findValue = model.FieldByName(field).String()
		dbField, err := crudApi.getDBFieldFromModel(field)
		if err != nil {
			return err
		}
		err = cl.FindByKV(c, dbField, findValue).All(c, &value)
		if err != nil && err != mongo.ErrNoDocuments {
			return err
		}
		if value != nil || len(value) > 0 {
			return errors.New("field already exists")
		}
	}
	res := cl.Insert(c, model.Interface())
	if res.InsertedID == nil {
		return errors.New("failed to create")
	}
	return nil
}

func (crudApi *crud) readAll(c *gin.Context, cl generic.IFace, limit int64) ([]primitive.M, error) {
	var results []primitive.M
	err := cl.GetAll(c, limit).All(c, &results)
	if err != nil && err != mongo.ErrNoDocuments && err != mongo.ErrNilCursor {
		return nil, err
	}
	return results, err
}

func (crudApi *crud) readById(c *gin.Context, cl generic.IFace, id string) (interface{}, error) {
	var value primitive.M
	err := cl.FindById(c, id).Decode(&value)
	if err != nil {
		return value, err
	}
	return value, err
}

func (crudApi *crud) readByKV(c *gin.Context, cl generic.IFace, k string, v interface{}) (interface{}, error) {
	var value []primitive.M
	err := cl.FindByKV(c, k, v).All(c, &value)
	if err != nil && err != mongo.ErrNoDocuments {
		return value, err
	}
	return value, err
}

func (crudApi *crud) update(c *gin.Context, cl generic.IFace, id string, updateParams interface{}) error {
	err := cl.Update(c, id, updateParams)
	return err
}

func (crudApi *crud) delete(c *gin.Context, cl generic.IFace, id string) error {
	deletedCount := cl.Delete(c, id)
	if deletedCount.DeletedCount != 1 {
		return errors.New("could not delete")
	}
	return nil
}

func (crudApi *crud) ownerCheck(c *gin.Context, cl generic.IFace, id string) (bool, error) {
	// get current logged in user id
	claims := auth.GetClaims(c)
	value, err := crudApi.readById(c, cl, id)
	if err != nil {
		return false, err
	}
	temp, err := bson.MarshalExtJSON(value, true, true)
	if err != nil {
		log.Println("error marshaling to json")
		return false, err
	}
	var rm map[string]interface{}
	err = json.Unmarshal(temp, &rm)
	if err != nil {
		log.Println("error unmashalling json to map")
		return false, err
	}
	ownerIdField, err := crudApi.getDBFieldFromModel(crudApi.config.OwnerIdField)
	if err != nil {
		log.Println("error getting owner field from model")
		return false, err
	}
	if ownerId, ok := rm[ownerIdField]; ok {
		// user owns the item
		if ownerId == claims.UserId.Hex() {
			return true, nil
		}
	}
	return false, errors.New("user does not own item")
}

func (crudApi *crud) createReadByKVHandler(field string) func(c *gin.Context) {
	// if an error occurs use this custom message
	errMsg := fmt.Sprintf("error occurred reading by %s", field)
	return func(c *gin.Context) {
		var err error
		var payload ReadByKVPayload
		if err = c.Bind(&payload); err == nil {
			cl := crudApi.GetCollectionClient(c)
			elems, err := crudApi.readByKV(c, cl, field, payload.Value)
			if err != nil {
				log.Println(err)
				c.JSON(http.StatusBadRequest, gin.H{"message": errMsg})
				return
			}
			c.JSON(http.StatusOK, elems)
			return
		}
		c.JSON(http.StatusBadRequest, gin.H{"message": errMsg})
		return
	}
}

func (crudApi *crud) GetCreateHandler(resource string) types.SquidHandler {
	return types.SquidHandler{
		fmt.Sprintf("/%s/create", resource),
		"POST",
		crudApi.CreateHandler,
	}
}

func (crudApi *crud) GetReadAllHandler(resource string) types.SquidHandler {
	return types.SquidHandler{
		fmt.Sprintf("/%s", resource),
		"GET",
		crudApi.ReadAllHandler,
	}
}

func (crudApi *crud) GetReadByIdHandler(resource string) types.SquidHandler {
	return types.SquidHandler{
		fmt.Sprintf("/%s/:id", resource),
		"GET",
		crudApi.ReadByIdHandler,
	}
}

func (crudApi *crud) GetUpdateHandler(resource string) types.SquidHandler {
	return types.SquidHandler{
		fmt.Sprintf("/%s/update", resource),
		"PATCH",
		crudApi.UpdateHandler,
	}
}

func (crudApi *crud) GetDeleteHandler(resource string) types.SquidHandler {
	return types.SquidHandler{
		fmt.Sprintf("/%s/:id", resource),
		"DELETE",
		crudApi.DeleteHandler,
	}
}

func (crudApi *crud) GetReadByKVHandlers(resource string) []types.SquidHandler {
	readHandlers := make([]types.SquidHandler, len(crudApi.config.FindableDBFields))
	for index, field := range crudApi.config.FindableDBFields {
		handler := crudApi.createReadByKVHandler(field)
		readHandlers[index] = types.SquidHandler{
			RelativePath: fmt.Sprintf("/find/%s/%s", resource, field),
			HTTPMethod:   "GET",
			Handler:      handler,
		}
	}
	return readHandlers
}

func (crudApi *crud) Handlers(resource string) []types.SquidHandler {
	readHandlers := crudApi.GetReadByKVHandlers(resource)
	return append([]types.SquidHandler{
		{
			fmt.Sprintf("/%s/create", resource),
			"POST",
			crudApi.CreateHandler,
		},
		{
			fmt.Sprintf("/%s", resource),
			"GET",
			crudApi.ReadAllHandler,
		},
		{
			fmt.Sprintf("/%s/:id", resource),
			"GET",
			crudApi.ReadByIdHandler,
		},
		{
			fmt.Sprintf("/%s/update", resource),
			"PATCH",
			crudApi.UpdateHandler,
		},
		{
			fmt.Sprintf("/%s/:id", resource),
			"DELETE",
			crudApi.DeleteHandler,
		},
	}, readHandlers...)
}

func (crudApi *crud) CreateHandler(c *gin.Context) {
	var err error
	payload := reflect.New(reflect.TypeOf(crudApi.config.Model)).Interface()
	if err = c.BindJSON(payload); err == nil {
		// create the model
		m := reflect.New(reflect.TypeOf(crudApi.config.Model)).Elem()
		v := reflect.ValueOf(payload).Elem()
		m.Set(v)
		if crudApi.config.OwnerIdField != "" {
			claims := auth.GetClaims(c)
			m.FieldByName(crudApi.config.OwnerIdField).SetString(claims.UserId.Hex())
		}
		cl := crudApi.GetCollectionClient(c)
		err := crudApi.create(c, cl, m)
		if err != nil {
			log.Println(err)
			c.JSON(http.StatusBadRequest, gin.H{"message": "error creating"})
			return
		}
		c.JSON(http.StatusCreated, gin.H{"message": "created"})
		return
	}
	c.JSON(http.StatusBadRequest, gin.H{"message": "error creating"})
	return
}

func (crudApi *crud) ReadByIdHandler(c *gin.Context) {
	var err error
	var payload ReadByIdPayload
	if err = c.BindUri(&payload); err == nil {
		cl := crudApi.GetCollectionClient(c)
		elem, err := crudApi.readById(c, cl, payload.Id)
		if err != nil {
			log.Println(err)
			c.JSON(http.StatusBadRequest, gin.H{"message": "error occurred reading by id"})
			return
		}
		result := reflect.New(reflect.TypeOf(crudApi.config.Model)).Interface()
		err = mapstructure.Decode(elem, &result)
		if err != nil {
			log.Println("error occurred decoding map into api result")
			c.JSON(http.StatusBadRequest, gin.H{"message": "error occurred reading by id"})
			return
		}
		c.JSON(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusBadRequest, gin.H{"message": "error reading by id"})
	return
}

func (crudApi *crud) ReadAllHandler(c *gin.Context) {
	var err error
	var payload ReadAllPayload
	if err = c.Bind(&payload); err == nil {
		cl := crudApi.GetCollectionClient(c)
		elements, err := crudApi.readAll(c, cl, payload.Limit)
		if err != nil {
			log.Println(err)
			c.JSON(http.StatusBadRequest, gin.H{"message": "error occurred fetching all"})
			return
		}
		c.JSON(http.StatusOK, elements)
		return
	}
	c.JSON(http.StatusBadRequest, gin.H{"message": "error occurred fetching all"})
	return
}

func (crudApi *crud) UpdateHandler(c *gin.Context) {
	var err error
	var payload UpdatePayload
	if err = c.BindJSON(&payload); err == nil {
		// validation
		for k, _ := range payload.UpdateParams {
			_, exists := crudApi.config.updateableFieldSet[k]
			if !exists {
				c.JSON(http.StatusPreconditionFailed, gin.H{"message": "invalid field in update"})
				return
			}
		}
		cl := crudApi.GetCollectionClient(c)
		// do an owner check to make sure the user updating the field owns it
		if crudApi.config.OwnerIdField != "" && !crudApi.config.OwnerCheckOptOut {
			_, err := crudApi.ownerCheck(c, cl, payload.Id)
			if err != nil {
				log.Println("owners check error", err.Error())
				c.JSON(http.StatusPreconditionFailed, gin.H{"message": "failed to do owners check"})
				return
			}
		}
		err = crudApi.update(c, cl, payload.Id, payload.UpdateParams)
		if err != nil {
			log.Println(err)
			c.JSON(http.StatusBadRequest, gin.H{"message": "error updating"})
			return
		}
		c.JSON(http.StatusOK, gin.H{"message": "updated item"})
		return
	}
	c.JSON(http.StatusBadRequest, gin.H{"message": "error updating"})
	return
}

func (crudApi *crud) DeleteHandler(c *gin.Context) {
	var err error
	var payload DeletePayload
	if err = c.BindUri(&payload); err == nil {
		cl := crudApi.GetCollectionClient(c)
		// do an owner check to make sure the user deleting the field owns it
		if crudApi.config.OwnerIdField != "" && !crudApi.config.OwnerCheckOptOut {
			_, err := crudApi.ownerCheck(c, cl, payload.Id)
			if err != nil {
				log.Println("owners check error", err.Error())
				c.JSON(http.StatusPreconditionFailed, gin.H{"message": "failed to do owners check"})
				return
			}
		}
		err = crudApi.delete(c, cl, payload.Id)
		if err != nil {
			log.Println(err)
			c.JSON(http.StatusBadRequest, gin.H{"message": "error deleting"})
			return
		}
		c.JSON(http.StatusOK, gin.H{"message": "deleted"})
		return
	}
	c.JSON(http.StatusBadRequest, gin.H{"message": "error deleting"})
	return
}

func NewCrudAPI(config *CrudConfig) *crud {
	// build set
	set := make(map[string]bool)
	for _, v := range config.UpdateableDBFields {
		set[v] = true
	}
	config.updateableFieldSet = set
	return &crud{
		config,
	}
}
