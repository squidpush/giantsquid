package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type UserProfile struct {
	Id           primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	UserId       primitive.ObjectID `json:"user_id" bson:"_user_id,omitempty"`
	GitHubId 	 string 			`json:"githubId" bson:"githubId"`
	Name         string             `json:"name" bson:"name"`
	Bio          string             `json:"bio" bson:"bio"`
	Company      string             `json:"company" bson:"company"`
	LocatedAt    string             `json:"located_at" bson:"located_at"`
	PhotoUrl     string             `json:"photo_url" bson:"photo_url"`
	WebsiteUrl   string             `json:"website_url" bson:"website_url"`
	Occupation   string             `json:"occupation" bson:"occupation"`
	Organization string             `json:"organization" bson:"organization"`
	BirthDate    primitive.DateTime `json:"birthdate" bson:"birthdate"`
}
