package models

type Config struct {
	DatabaseName     string   `json:"database_name" binding:"required"`
	DatabaseShards   []string `json:"database_shards" binding:"required"`
	DatabaseUsername string   `json:"database_username" binding:"required"`
	DatabasePassword string   `json:"database_password" binding:"required"`
	DatabaseSource   string   `json:"database_source" binding:"required"`
	Hostname         string   `json:"hostname" binding:"required"`
	Domain           string   `json:"domain" binding:"required"`
	AllowedOrigins   []string `json:"allowed_origins" binding:"required"`
}
