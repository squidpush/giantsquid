package helpers

import (
	"crypto/rand"
	"io"
	"log"
)

const saltBytes = 32

func GenSalt() []byte {
	salt := make([]byte, saltBytes)
	_, err := io.ReadFull(rand.Reader, salt)
	if err != nil {
		log.Fatal(err)
	}
	return salt
}
