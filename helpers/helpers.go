package helpers

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/squidpush/giantsquid/repositories/generic"
	"go.mongodb.org/mongo-driver/mongo"
)

func GetCollectionClient(c *gin.Context, collection string) (generic.IFace, error) {
	var mongoClient *mongo.Client
	var dbName string
	client, exists :=  c.Get("dbClient")
	mongoClient = client.(*mongo.Client)
	if !exists {
		return nil, errors.New("client does not exist")
	}
	k, exists :=  c.Get("dbName")
	if !exists {
		// default to squad
		dbName = "squad"
	}
	if v, ok := k.(string); ok {
		dbName = v
	} else {
		// default to squad
		dbName = "squad"
	}
	return generic.NewGenericClient(mongoClient.Database(dbName).Collection(collection), mongoClient), nil
}