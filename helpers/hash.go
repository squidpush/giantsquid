package helpers

import (
	"bytes"
	"golang.org/x/crypto/argon2"
)

// Parameters as per the RFC
const hashTime = 1
const memory = 64*1024
const threads = 4
const keyLen = 32

func GenHash(rawPassword []byte, salt []byte) []byte {
	return argon2.IDKey(rawPassword, salt, hashTime, memory, threads, keyLen)
}

func CompareHash(password []byte, rawPassword []byte, salt []byte) bool {
	return bytes.Equal(GenHash(rawPassword, salt), password)
}