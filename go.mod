module gitlab.com/squidpush/giantsquid

go 1.13

require (
	github.com/appleboy/gin-jwt/v2 v2.6.3
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/autotls v0.0.0-20200314141124-cc69476aef2a
	github.com/gin-gonic/gin v1.6.2
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/google/go-github v17.0.0+incompatible
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/mitchellh/mapstructure v1.1.2
	github.com/stretchr/testify v1.5.1
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.3.1
	go.uber.org/zap v1.14.0
	golang.org/x/crypto v0.0.0-20200220183623-bac4c82f6975
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
)
