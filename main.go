package main

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/squidpush/giantsquid/server"
	"gitlab.com/squidpush/giantsquid/types"
	"net/http"
)

func main() {
	cc := cors.DefaultConfig()
	cc.AllowCredentials = true
	cc.AllowOrigins = []string{"*"}
	// Test out passing a config down
	config := types.SquidConfig{
		PublicHandlers:  []types.SquidHandler{
			{
				RelativePath: "/ping",
				HTTPMethod:   "GET",
				Handler:      func(c *gin.Context) {
					c.JSON(http.StatusOK, gin.H {
						"message": "pong",
					})
					return
				},
			},
		},
		PrivateHandlers: []types.SquidHandler{},
		UseMongoDB: true,
		SignUpHandler: nil,
		GitHubOauthConfig: &types.SquidGitHubOauthConfig{
			Scopes: []string{"user", "repo"},
			UseEnv: false,
		},
		CookieAuthConfig: &types.SquidCookieAuthConfig{
			CookieName: "token",
			CookieHTTPOnly: true,
			CookieDomain: "dev.localhost",
			SendCookie: true,
			SecureCookie: false,
			TokenLookup: "cookie:token",
		},
		CorsConfig: &cc,
	}
	s := server.NewServerClient(config)
	s.Start()
}

