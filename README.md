# giantsquid

## Description

giantsquid is a high performance, opinionated Golang server library, batteries included.

## Built for expansion

giantsquid supports:
- Authenticated routes
- Database repositories (currently MongoDB config included)
- Unified service configuration
- Multiple cloud deployment

## How to Use

```go
// Define a handler as you'd do normally in gin/gonic
privateHandler := func(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"Message": "Test handler",
	})
	return
}

// Define a configuration
serverConfig := SquidConfig{
    PublicHandlers: nil,
    PrivateHandlers: []SquidHandler{
        {
            "/hello",
            "GET",
            privateHandler,
        },
    },
    UseMongoDB: false,
}

// Create a server client and start the server, it normally runs on port 8080
// You can override the port by providing a $PORT environment variable
serverClient := NewServerClient(serverConfig)
serverClient.Start()

```


## Configure MongoDB

Set the `MONGO_CONN_STR` environment variable to configure a mongo connection string, by default if you have MongoDB on
it will use "mongodb://127.0.0.1:27017"

The default db name is `squad` (a group of squids), if you want to change this set the `MONGO_DBNAME` environment variable


## How to Contribute

Open a merge request with a description of your proposed feature and the code change. 
