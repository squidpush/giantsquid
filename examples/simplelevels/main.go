package simplelevels

import (
	"gitlab.com/squidpush/giantsquid/generators"
	"gitlab.com/squidpush/giantsquid/server"
	"gitlab.com/squidpush/giantsquid/types"
)

type Level struct {
	Name        string `json:"name" bson:"name"`
	Description string `json:"description" bson:"description"`
	OwnerId     string `json:"ownerId" bson:"ownerId"`
	Difficulty  int64  `json:"difficulty" bson:"difficulty"`
	Slug 		string `json:"slug" bson:"slug"`
}

func main() {
	cc := generators.CrudConfig{
		Collection:       "levels",
		UniqueFields:     []string{"Slug"},
		Model:            Level{},
		UpdateableDBFields: []string{"name", "description"},
		FindableDBFields: []string{"name"},
		OwnerIdField: "OwnerId",
	}
	cl := generators.NewCrudAPI(&cc)
	handlers := cl.Handlers("levels")
	// Test out passing a config down
	config := types.SquidConfig{
		PublicHandlers:  nil,
		PrivateHandlers: handlers,
		UseMongoDB:      true,
	}
	s := server.NewServerClient(config)
	s.Start()
}