package iot

import (
	"gitlab.com/squidpush/giantsquid/generators"
	"gitlab.com/squidpush/giantsquid/server"
	"gitlab.com/squidpush/giantsquid/types"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Sensor struct {
	Id          primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	LocationId  string             `json:"locationId" bson:"locationId"`
	Name        string             `json:"name" bson:"name"`
	Tag         string             `json:"tag" bson:"tag"`
	Description string             `json:"description" bson:"description"`
}

type SensorData struct {
	Id          primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Temperature float64            `json:"temperature" bson:"temperature"`
	Humidity    float64            `json:"humidity" bson:"humidity"`
	SensorId    string             `json:"sensorId" bson:"sensorId"`
}

type Location struct {
	Id      primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Name    string             `json:"name" bson:"name"`
	Address string             `json:"address" bson:"address"`
	OwnerId string             `json:"ownerId" bson:"ownerId"`
	Tag     string             `json:"tag" bson:"tag"`
}

func main() {
	sensorCrudConfig := &generators.CrudConfig{
		Collection:         "sensors",
		UniqueFields:       []string{"Tag"},
		Model:              Sensor{},
		UpdateableDBFields: []string{"name", "description"},
		FindableDBFields:   []string{"tag"},
	}
	sensorDataCrudConfig := &generators.CrudConfig{
		Collection: "sensorData",
		Model:      SensorData{},
	}
	locationDataCrudConfig := &generators.CrudConfig{
		Collection:         "locations",
		UniqueFields:       []string{"Tag"},
		Model:              Location{},
		OwnerIdField:       "OwnerId",
		UpdateableDBFields: []string{"name", "address"},
		FindableDBFields:   []string{"tag"},
	}
	sensorCrud := generators.NewCrudAPI(sensorCrudConfig)
	sensorDataCrud := generators.NewCrudAPI(sensorDataCrudConfig)
	locationCrud := generators.NewCrudAPI(locationDataCrudConfig)

	handlers := append(sensorCrud.Handlers("sensors"), sensorDataCrud.Handlers("sensor-data")...)
	handlers = append(handlers, locationCrud.Handlers("locations")...)
	serverConfig := types.SquidConfig{
		PublicHandlers:  nil,
		PrivateHandlers: handlers,
		UseMongoDB:      true,
	}
	sc := server.NewServerClient(serverConfig)
	sc.Start()
}
