package server

import (
	"gitlab.com/squidpush/giantsquid/types"
)

type IFace interface {
	Start()
	getPort() string
	configLoggerMiddleware()
	configDBMiddleware()
	configRoutes()
}

func NewServerClient(cfg types.SquidConfig) IFace {
	return newServerClient(cfg)
}
