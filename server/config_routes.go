package server

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/squidpush/giantsquid/middlewares/auth"
	"time"
)

func (s *SquidServer) configRoutes() {

	v1 := s.engine.Group("/v1")
	squidAuthMiddelware := auth.Middleware{
		Realm:   "squidserver",
		Timeout: time.Hour,
	}

	if s.config.CookieAuthConfig != nil {
		squidAuthMiddelware = auth.Middleware{
			Realm:          "squidserver",
			Timeout:        time.Hour,
			CookieHTTPOnly: s.config.CookieAuthConfig.CookieHTTPOnly,
			SendCookie:     s.config.CookieAuthConfig.SendCookie,
			SecureCookie:   s.config.CookieAuthConfig.SecureCookie,
			CookieDomain:   s.config.CookieAuthConfig.CookieDomain,
			CookieName:     s.config.CookieAuthConfig.CookieName,
			TokenLookup:    s.config.CookieAuthConfig.TokenLookup,
		}
	}

	for _, handler := range s.config.PublicHandlers {
		v1.Handle(handler.HTTPMethod, handler.RelativePath, handler.Handler)
	}

	if s.config.SignUpHandler != nil {
		handler := s.config.SignUpHandler
		v1.Handle(handler.HTTPMethod, handler.RelativePath, handler.Handler)
	} else {
		// use the default signup handler if a custom signup handler is not defined
		v1.POST("/signup", auth.SignUpHandler)
	}

	am := squidAuthMiddelware.GetAuthMiddleware()

	v1.POST("/login", am.LoginHandler)
	v1.POST("/logout", am.LogoutHandler)

	s.engine.NoRoute(am.MiddlewareFunc(), func(c *gin.Context) {
		c.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Page not found"})
	})

	privateV1 := s.engine.Group("/private/v1")

	// Refresh time can be longer than token timeout
	privateV1.GET("/refresh_token", am.RefreshHandler)
	privateV1.Use(am.MiddlewareFunc())
	{
		privateV1.GET("/auth_ping", func(c *gin.Context) {
			c.JSON(200, gin.H{
				"message": fmt.Sprintf("Authenticated pong for: %s", auth.GetNameFromClaims(c)),
			})
		})
		for _, handler := range s.config.PrivateHandlers {
			privateV1.Handle(handler.HTTPMethod, handler.RelativePath, handler.Handler)
		}
	}

}
