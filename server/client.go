package server

import (
	"github.com/gin-gonic/autotls"
	"gitlab.com/squidpush/giantsquid/middlewares/auth/oauth2/github"
	"gitlab.com/squidpush/giantsquid/middlewares/cors"
	"gitlab.com/squidpush/giantsquid/types"
	"log"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/squidpush/giantsquid/middlewares/mongodb"
	"go.uber.org/zap"
)

type SquidServer struct {
	engine *gin.Engine
	config types.SquidConfig
}

// Start turns on SquidServer
func (s *SquidServer) Start() {
	s.configDBMiddleware()
	s.configLoggerMiddleware()
	s.configCorsMiddleware()
	s.configGithubOauth()
	s.configRoutes()
	var err error
	if s.config.AutoTLSConfig != nil {
		log.Println("Running with auto tls")
		err = autotls.Run(s.engine, s.config.AutoTLSConfig.Domains...)
	}
	err = s.engine.Run(s.getPort())
	if err != nil {
		logger, _ := s.getLogger()
		logger.Error(err.Error())
	}
}

func (s *SquidServer) getLogger() (*zap.Logger, error) {
	return zap.NewProduction()
}

func (s *SquidServer) configGithubOauth() {
	if s.config.GitHubOauthConfig != nil {
		timeDuration := time.Hour
		if s.config.GitHubOauthConfig.JWTDuration == nil {
			s.config.GitHubOauthConfig.JWTDuration = &timeDuration
		}
		if s.config.CookieAuthConfig != nil {
			s.config.GitHubOauthConfig.CookieAuthConfig = s.config.CookieAuthConfig
		}
		ng := github.NewGitHubOauth(
			s.config.GitHubOauthConfig.Scopes,
			s.config.GitHubOauthConfig.UseEnv,
			s.config.GitHubOauthConfig.JWTDuration,
			s.config.GitHubOauthConfig.CookieAuthConfig)
		s.engine.GET("/ghlogin", ng.RedirectToGitHubHandler)
		s.engine.GET("/ghauth", ng.GitHubGrantHandler)
	}
}

func (s *SquidServer) configLoggerMiddleware() {
	logger, err := s.getLogger()
	if err != nil {
		log.Println("failed to get router with err: ", err.Error())
	}
	s.engine.Use(func(c *gin.Context) {
		c.Set("logger", logger)
		if len(c.Errors.Errors()) > 1 {
			defer func() {
				_ = logger.Sync()
			}()
			sugar := logger.Sugar()
			sugar.Error(c.Errors.Errors())
		}
		c.Next()
	})
}

func (s *SquidServer) configCorsMiddleware() {
	if s.config.CorsConfig != nil {
		cm := cors.GetCorsMiddleware(s.config.CorsConfig)
		s.engine.Use(cm)
	}
}

func (s *SquidServer) configDBMiddleware() {
	if s.config.UseMongoDB {
		p := os.Getenv("MONGO_CONN_STR")
		if p == "" {
			p = "mongodb://127.0.0.1:27017"
		}
		d := os.Getenv("MONGO_DBNAME")
		log.Println(d)
		if d == "" {
			d = "squad"
		}
		middleware := mongodb.NewMongoMiddleware(p, d)
		s.engine.Use(middleware.GetMongoMiddleware())
	}
}

func (s *SquidServer) getPort() string {
	p := os.Getenv("PORT")
	if p != "" {
		return ":" + p
	}
	return ":8080"
}

func newServerClient(cfg types.SquidConfig) *SquidServer {
	return &SquidServer{gin.Default(), cfg}
}
