package server

import (
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/require"
	"gitlab.com/squidpush/giantsquid/types"
	"net/http"
	"testing"
	"time"
)

func SetupTest() {
	privateHandler := func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"Message": "Test handler",
		})
		return
	}
	serverConfig := types.SquidConfig{
		PublicHandlers: nil,
		PrivateHandlers: []types.SquidHandler{
			{
				"/hello",
				"GET",
				privateHandler,
			},
		},
		UseMongoDB: false,
	}
	serverClient := NewServerClient(serverConfig)
	serverClient.Start()
}
func TestPrivateRoutesHandled(t *testing.T) {
	go func() {
		SetupTest()
	}()
	time.Sleep(2000)
	resp, err := http.Get("http://localhost:8080/v1/private/hello")
	require.Nil(t, err)
	require.Equal(t, resp.StatusCode, http.StatusUnauthorized)
}
