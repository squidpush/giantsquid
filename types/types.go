package types

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"time"
)

type SquidTLSConfig struct {
	// Domains used for AutoTLS configuration
	Domains []string
}

type SquidHandler struct {
	RelativePath string
	HTTPMethod   string
	Handler      func(ctx *gin.Context)
}

type SquidCookieAuthConfig struct {
	CookieDomain string
	CookieHTTPOnly bool
	SecureCookie bool
	SendCookie bool
	CookieName string
	TokenLookup string
}

type SquidGitHubOauthConfig struct {
	// Github Scopes
	Scopes []string
	// Github Use Env to not opt to use a json file
	UseEnv bool
	// Duration for jwt
	JWTDuration *time.Duration
	// CookieAuthConfig
	CookieAuthConfig *SquidCookieAuthConfig
}

type SquidConfig struct {
	// Configuration for publicly available routes
	PublicHandlers []SquidHandler
	// Configuration for private scoped routes
	PrivateHandlers []SquidHandler
	// Opt in for using MongoDB as the primary db provider
	UseMongoDB bool
	// Optional custom sign up handler configuration
	SignUpHandler *SquidHandler
	// Optional cors middleware
	CorsConfig *cors.Config
	// Optional TLS setting
	AutoTLSConfig *SquidTLSConfig
	// GitHubConfig
	GitHubOauthConfig *SquidGitHubOauthConfig
	// SquidCookieAuthConfig
	CookieAuthConfig *SquidCookieAuthConfig
}